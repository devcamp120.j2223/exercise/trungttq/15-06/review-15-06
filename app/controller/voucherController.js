const prizeModel = require("../models/voucherModel");

const mongoose = require("mongoose");
const voucherModel = require("../models/voucherModel");

const createVoucher = (req, res) => {
    let body = req.body;
    if (!body.code) {
        res.status(400).json({
            message: `code is require`
        })
    } else if (!body.discount) {
        res.status(400).json({
            message: `discount is require`
        })
    } else {
        let voucher = {
            _id: mongoose.Types.ObjectId(),
            code: body.code,
            discount: body.discount,
            note: body.note,
            createdAt: body.createdAt,
            updatedAt: body.updatedAt
        }
        voucherModel.create(voucher, (err, data) => {
            if (err) {
                res.status(500).json({
                    message: err.message
                })
            } else {
                res.status(201).json({
                    message: `create voucher successfully`,
                    data: data
                })
            }
        })
    }
};

const getAllVoucher = (req, res) => {
    voucherModel.find((err, data) => {
        if (err) {
            res.status(500).json({
                message: err.message
            })
        } else {
            res.status(201).json({
                message: `get all voucher successfully`,
                data: data
            })
        }
    })
};

const getVoucherById = (req, res) => {
    let id = req.params.voucherId;
    if (!mongoose.Types.ObjectId.isValid(id)) {
        res.status(400).json({
            message: `voucherId is invalid`
        })
    } else {
        voucherModel.findById(id, (err, data) => {
            if (err) {
                res.status(500).json({
                    message: err.message
                })
            } else {
                res.status(201).json({
                    message: `get voucher by id successfully`,
                    data: data
                })
            }
        })
    }
};

const updateVoucherById = (req, res) => {
    let id = req.params.voucherId;
    let body = req.body;
    if (!mongoose.Types.ObjectId(id)) {
        res.status(400).json({
            message: `prizeId is invalid`
        })
    } else if (!body.code) {
        res.status(400).json({
            message: `code is require`
        })
    } else if (!body.discount) {
        res.status(400).json({
            message: `discount is require`
        })
    } else if(!Number.isInteger(body.discount)){
        res.status(400).json({
            message: `discount is invalid`
        })
    }else {
        let voucher = {
            code: body.code,
            discount: body.discount,
            note: body.note,
            createdAt: body.createdAt,
            updatedAt: body.updatedAt
        }
        voucherModel.findByIdAndUpdate(id, voucher, (err, data) => {
            if (err) {
                res.status(500).json({
                    message: err.message
                })
            } else {
                res.status(201).json({
                    message: `update voucher successfully`,
                    data: data
                })
            }
        })
    }
};

const deleteVoucherById = (req, res) => {
    let id = req.params.voucherId;
    if (!mongoose.Types.ObjectId.isValid(id)) {
        res.status(400).json({
            message: `voucherId is invalid`
        })
    } else {
        voucherModel.findByIdAndDelete(id, (err, data) => {
            if (err) {
                res.status(500).json({
                    message: err.message
                })
            } else {
                res.status(204).json({
                    message: `delete voucher by id successfully`,
                    data: data
                })
            }
        })
    }
};

module.exports = { createVoucher, getAllVoucher, getVoucherById, updateVoucherById, deleteVoucherById };