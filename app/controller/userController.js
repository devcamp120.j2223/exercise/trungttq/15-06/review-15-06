const userModel = require("../models/userModel");

const mongoose = require("mongoose");

const createUser = (req, res) => {
    let body = req.body;
    if(!body.username){
        res.status(400).json({
            message: `username is require`
        })
    }else if(!body.firstname){
        res.status(400).json({
            message: `firstname is require`
        })
    }else if(!body.lastname){
        res.status(400).json({
            message: `lastname is require`
        })
    }else{
        let user = {
            _id: mongoose.Types.ObjectId(),
            username: body.username,
            firstname: body.firstname,
            lastname: body.lastname,
        }
        userModel.create(user, (err, data) => {
            if(err){
                res.status(500).json({
                    message: err.message
                })
            }else{
                res.status(201).json({
                    message: `create user successfully`,
                    data: data
                })
            }
        })
    }
};

const getAllUser = (req, res) => {
    userModel.find((err, data) => {
        if(err){
            res.status(500).json({
                message: err.message
            })
        }else{
            res.status(201).json({
                message: `get all user successfully`,
                data: data
            })
        }
    })
};

const getUserById = (req, res) => {
    let id = req.params.userid;
    if(!mongoose.Types.ObjectId.isValid(id)){
        res.status(400).json({
            message: `userid is invalid`
        })
    }else{
        userModel.findById(id, (err, data) => {
            if(err){
                res.status(500).json({
                    message: err.message
                })
            }else{
                res.status(201).json({
                    message: `get user by id successfully`,
                    data: data
                })
            }
        })
    }
};

const updateUserById = (req, res) => {
    let id = req.params.userid;
    let body = req.body;
    if(!mongoose.Types.ObjectId(id)){
        res.status(400).json({
            message: `userid is invalid`
        })
    }
    else if(!body.username){
        res.status(400).json({
            message: `username is require`
        })
    }else if(!body.firstname){
        res.status(400).json({
            message: `firstname is require`
        })
    }else if(!body.lastname){
        res.status(400).json({
            message: `lastname is require`
        })
    }else{
        let user = {
            username: body.username,
            firstname: body.firstname,
            lastname: body.lastname,
        }
        userModel.findByIdAndUpdate(id,user, (err, data) => {
            if(err){
                res.status(500).json({
                    message: err.message
                })
            }else{
                res.status(201).json({
                    message: `update user successfully`,
                    data: data
                })
            }
        })
    }
};

const deleteUserById = (req, res) => {
    let id = req.params.userid;
    if(!mongoose.Types.ObjectId.isValid(id)){
        res.status(400).json({
            message: `userid is invalid`
        })
    }else{
        userModel.findByIdAndDelete(id, (err, data) => {
            if(err){
                res.status(500).json({
                    message: err.message
                })
            }else{
                res.status(204).json({
                    message: `delete user by id successfully`,
                    data: data
                })
            }
        })
    }
};

module.exports = {createUser, getAllUser, getUserById, updateUserById, deleteUserById};