const voucherHistoryModel = require("../models/voucherHistoryModel");

const mongoose = require("mongoose");

const createVoucherHistory = (req, res) => {
    let body = req.body;
    if(!body.user){
        res.status(400).json({
            message: `user is require`
        })
    }else if(!mongoose.Types.ObjectId(body.user)){
        res.status(400).json({
            message: `user is invalid`
        })
    }else if(!body.voucher){
        res.status(400).json({
            message: `voucher is require`
        })
    }else if(!mongoose.Types.ObjectId(body.voucher)){
        res.status(400).json({
            message: `voucher is invalid`
        })
    }else{
        let voucherHistory = {
            _id: mongoose.Types.ObjectId(),
            user: body.user,
            voucher: body.voucher,
            createdAt: body.createdAt,
            updatedAt: body.updatedAt
            
        }
        voucherHistoryModel.create(voucherHistory, (err, data) => {
            if(err){
                res.status(500).json({
                    message: err.message
                })
            }else{
                res.status(201).json({
                    message: `create voucherHistory successfully`,
                    data: data
                })
            }
        })
    }
};

const getAllVoucherHistory = (req, res) => {
    voucherHistoryModel.find((err, data) => {
        if(err){
            res.status(500).json({
                message: err.message
            })
        }else{
            res.status(201).json({
                message: `get all voucherhistory successfully`,
                data: data
            })
        }
    })
};

const getAllUserVoucherHistory = (req, res) => {
    let user = req.query.user;
    console.log(user);
    let condition = {};
    if(user){
        condition.user = user;
    }
    voucherHistoryModel.find(condition,(err, data) => {
        if(err){
            res.status(500).json({
                message: err.message
            })
        }else{
            res.status(201).json({
                message: `get all user voucherhistory successfully`,
                data: data
            })
        }
    })
};

const getVoucherHistoryById = (req, res) => {
    let id = req.params.historyId;
    if(!mongoose.Types.ObjectId.isValid(id)){
        res.status(400).json({
            message: `voucherHistoryId is invalid`
        })
    }else{
        voucherHistoryModel.findById(id, (err, data) => {
            if(err){
                res.status(500).json({
                    message: err.message
                })
            }else{
                res.status(201).json({
                    message: `get voucherhistory by id successfully`,
                    data: data
                })
            }
        })
    }
};

const updateVoucherHistoryById = (req, res) => {
    let id = req.params.historyId;
    let body = req.body;
    if(!mongoose.Types.ObjectId(id)){
        res.status(400).json({
            message: `historyid is invalid`
        })
    }
    if(!body.user){
        res.status(400).json({
            message: `user is require`
        })
    }else if(!mongoose.Types.ObjectId(body.user)){
        res.status(400).json({
            message: `user is invalid`
        })
    }else if(!body.voucher){
        res.status(400).json({
            message: `voucher is require`
        })
    }else if(!mongoose.Types.ObjectId(body.voucher)){
        res.status(400).json({
            message: `voucher is invalid`
        })
    }else{
        let voucherHistory = {
           
            user: body.user,
            voucher: body.voucher,
            createdAt: body.createdAt,
            updatedAt: body.updatedAt
            
        }
        voucherHistoryModel.findByIdAndUpdate(id,voucherHistory, (err, data) => {
            if(err){
                res.status(500).json({
                    message: err.message
                })
            }else{
                res.status(201).json({
                    message: `update voucherhistory successfully`,
                    data: data
                })
            }
        })
    }
};

const deleteVoucherHistoryById  = (req, res) => {
    let id = req.params.historyId;
    if(!mongoose.Types.ObjectId.isValid(id)){
        res.status(400).json({
            message: `voucherHistoryId is invalid`
        })
    }else{
        voucherHistoryModel.findByIdAndDelete(id, (err, data) => {
            if(err){
                res.status(500).json({
                    message: err.message
                })
            }else{
                res.status(204).json({
                    message: `delete voucherhistory by id successfully`,
                    data: data
                })
            }
        })
    }
};

module.exports = {createVoucherHistory , getAllVoucherHistory , getVoucherHistoryById , updateVoucherHistoryById , deleteVoucherHistoryById, getAllUserVoucherHistory };