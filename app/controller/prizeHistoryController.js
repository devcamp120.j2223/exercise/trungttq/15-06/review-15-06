const prizeHistoryModel = require("../models/prizeHistoryModel");

const mongoose = require("mongoose");

const createPrizeHistory = (req, res) => {
    let body = req.body;
    if(!body.user){
        res.status(400).json({
            message: `user is require`
        })
    }else if(!mongoose.Types.ObjectId(body.user)){
        res.status(400).json({
            message: `user is invalid`
        })
    }else if(!body.prize){
        res.status(400).json({
            message: `prize is require`
        })
    }else if(!mongoose.Types.ObjectId(body.prize)){
        res.status(400).json({
            message: `prize is invalid`
        })
    }else{
        let prizeHistory = {
            _id: mongoose.Types.ObjectId(),
            user: body.user,
            prize: body.prize,
            createdAt: body.createdAt,
            updatedAt: body.updatedAt
            
        }
        prizeHistoryModel.create(prizeHistory, (err, data) => {
            if(err){
                res.status(500).json({
                    message: err.message
                })
            }else{
                res.status(201).json({
                    message: `create prizehistory successfully`,
                    data: data
                })
            }
        })
    }
};

const getAllPrizeHistory = (req, res) => {
    prizeHistoryModel.find((err, data) => {n
        if(err){
            res.status(500).json({
                message: err.message
            })
        }else{
            res.status(201).json({
                message: `get all prizehistory successfully`,
                data: data
            })
        }
    })
};

const getAllUserPrizeHistory = (req, res) => {
    let user = req.query.user;
    console.log(user);
    let condition = {};
    if(user){
        condition.user = user;
    }
    prizeHistoryModel.find(condition,(err, data) => {
        if(err){
            res.status(500).json({
                message: err.message
            })
        }else{
            res.status(201).json({
                message: `get all user prizehistory successfully`,
                data: data
            })
        }
    })
};

const getPrizeHistoryById = (req, res) => {
    let id = req.params.historyId;
    if(!mongoose.Types.ObjectId.isValid(id)){
        res.status(400).json({
            message: `prizeHistoryId is invalid`
        })
    }else{
        prizeHistoryModel.findById(id, (err, data) => {
            if(err){
                res.status(500).json({
                    message: err.message
                })
            }else{
                res.status(201).json({
                    message: `get prizehistory by id successfully`,
                    data: data
                })
            }
        })
    }
};

const updatePrizeHistoryById = (req, res) => {
    let id = req.params.historyId;
    let body = req.body;
    if(!mongoose.Types.ObjectId(id)){
        res.status(400).json({
            message: `historyid is invalid`
        })
    }
    if(!body.user){
        res.status(400).json({
            message: `user is require`
        })
    }else if(!mongoose.Types.ObjectId(body.user)){
        res.status(400).json({
            message: `user is invalid`
        })
    }else if(!body.prize){
        res.status(400).json({
            message: `prize is require`
        })
    }else if(!mongoose.Types.ObjectId(body.prize)){
        res.status(400).json({
            message: `prize is invalid`
        })
    }else{
        let prizeHistory = {
            
            user: body.user,
            prize: body.prize,
            createdAt: body.createdAt,
            updatedAt: body.updatedAt
            
        }
        prizeHistoryModel.findByIdAndUpdate(id,prizeHistory, (err, data) => {
            if(err){
                res.status(500).json({
                    message: err.message
                })
            }else{
                res.status(201).json({
                    message: `update prizehistory successfully`,
                    data: data
                })
            }
        })
    }
};

const deletePrizeHistoryById  = (req, res) => {
    let id = req.params.historyId;
    if(!mongoose.Types.ObjectId.isValid(id)){
        res.status(400).json({
            message: `prizeHistoryId is invalid`
        })
    }else{
        prizeHistoryModel.findByIdAndDelete(id, (err, data) => {
            if(err){
                res.status(500).json({
                    message: err.message
                })
            }else{
                res.status(204).json({
                    message: `delete prizehistory by id successfully`,
                    data: data
                })
            }
        })
    }
};

module.exports = {createPrizeHistory , getAllPrizeHistory , getPrizeHistoryById , updatePrizeHistoryById , deletePrizeHistoryById, getAllUserPrizeHistory };