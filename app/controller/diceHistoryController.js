const diceHistoryModel = require("../models/diceHistoryModel");

const mongoose = require("mongoose");

const createDiceHistory = (req, res) => {
    let body = req.body;
    if(!body.user){
        res.status(400).json({
            message: `user is require`
        })
    }else if(!mongoose.Types.ObjectId(body.user)){
        res.status(400).json({
            message: `user is invalid`
        })
    }else if(!body.dice){
        res.status(400).json({
            message: `dice is require`
        })
    }else if(!Number.isInteger(body.dice) || body.dice < 0 || body.dice >6){
        res.status(400).json({
            message: `dice is invalid`
        })
    }else{
        let diceHistory = {
            _id: mongoose.Types.ObjectId(),
            user: body.user,
            dice: body.dice
            
        }
        diceHistoryModel.create(diceHistory, (err, data) => {
            if(err){
                res.status(500).json({
                    message: err.message
                })
            }else{
                res.status(201).json({
                    message: `create dicehistory successfully`,
                    data: data
                })
            }
        })
    }
};

const getAllDiceHistory = (req, res) => {
    diceHistoryModel.find((err, data) => {
        if(err){
            res.status(500).json({
                message: err.message
            })
        }else{
            res.status(201).json({
                message: `get all dicehistory successfully`,
                data: data
            })
        }
    })
};

const getAllUserDiceHistory = (req, res) => {
    let user = req.query.user
    let condition = {};

    if(user){
        condition.user = user;
    }
    diceHistoryModel.find(condition,(err, data) => {
        if(err){
            res.status(500).json({
                message: err.message
            })
        }else{
            res.status(201).json({
                message: `get all dicehistory successfully`,
                data: data
            })
        }
    })
};

const getDiceHistoryById = (req, res) => {
    let id = req.params.diceHistoryId;
    if(!mongoose.Types.ObjectId.isValid(id)){
        res.status(400).json({
            message: `diceHistoryId is invalid`
        })
    }else{
        diceHistoryModel.findById(id, (err, data) => {
            if(err){
                res.status(500).json({
                    message: err.message
                })
            }else{
                res.status(201).json({
                    message: `get dicehistory by id successfully`,
                    data: data
                })
            }
        })
    }
};

const updateDiceHistoryById = (req, res) => {
    let id = req.params.diceHistoryId;
    let body = req.body;
    if(!mongoose.Types.ObjectId(id)){
        res.status(400).json({
            message: `userid is invalid`
        })
    }
    if(!body.user){
        res.status(400).json({
            message: `user is require`
        })
    }else if(!mongoose.Types.ObjectId(body.user)){
        res.status(400).json({
            message: `user is invalid`
        })
    }else if(!body.dice){
        res.status(400).json({
            message: `dice is require`
        })
    }else if(!Number.isInteger(body.dice) || body.dice < 0 || body.dice >6){
        res.status(400).json({
            message: `dice is invalid`
        })
    }else{
        let diceHistory = {
            user: body.user,
            dice: body.dice
            
        }
        diceHistoryModel.findByIdAndUpdate(id,diceHistory, (err, data) => {
            if(err){
                res.status(500).json({
                    message: err.message
                })
            }else{
                res.status(201).json({
                    message: `update dicehistory successfully`,
                    data: data
                })
            }
        })
    }
};

const deleteDiceHistoryById  = (req, res) => {
    let id = req.params.diceHistoryId;
    if(!mongoose.Types.ObjectId.isValid(id)){
        res.status(400).json({
            message: `diceHistoryId is invalid`
        })
    }else{
        diceHistoryModel.findByIdAndDelete(id, (err, data) => {
            if(err){
                res.status(500).json({
                    message: err.message
                })
            }else{
                res.status(204).json({
                    message: `delete dicehistory by id successfully`,
                    data: data
                })
            }
        })
    }
};

module.exports = {createDiceHistory , getAllDiceHistory , getDiceHistoryById , updateDiceHistoryById , deleteDiceHistoryById , getAllUserDiceHistory};