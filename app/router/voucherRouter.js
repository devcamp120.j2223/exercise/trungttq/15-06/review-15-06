const express = require("express");

const {createVoucher , getAllVoucher , getVoucherById , updateVoucherById , deleteVoucherById } = require("../controller/voucherController");

const router = express.Router();

router.get("/vouchers", getAllVoucher);
router.get("/vouchers/:voucherId", getVoucherById);
router.post("/vouchers",createVoucher);
router.put("/vouchers/:voucherId", updateVoucherById);
router.delete("/vouchers/:voucherId", deleteVoucherById);

module.exports = router;