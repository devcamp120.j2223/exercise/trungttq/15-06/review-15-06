const express = require("express");

const {createVoucherHistory , getAllVoucherHistory , getVoucherHistoryById , updateVoucherHistoryById , deleteVoucherHistoryById, getAllUserVoucherHistory } = require("../controller/voucherHistoryController");

const router = express.Router();

router.get("/voucher-histories", getAllVoucherHistory);
router.get("/voucher-user-histories", getAllUserVoucherHistory);
router.get("/voucher-histories/:historyId", getVoucherHistoryById );
router.post("/voucher-histories",createVoucherHistory);
router.put("/voucher-histories/:historyId", updateVoucherHistoryById );
router.delete("/voucher-histories/:historyId", deleteVoucherHistoryById );

module.exports = router;