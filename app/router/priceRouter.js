const express = require("express");

const {createPrize, getAllPrize, getPrizeById, updatePrizeById, deletePrizeById} = require("../controller/prizeController");

const router = express.Router();

router.get("/prizes", getAllPrize);
router.get("/prizes/:prizeId", getPrizeById);
router.post("/prizes",createPrize);
router.put("/prizes/:prizeId", updatePrizeById);
router.delete("/prizes/:prizeId", deletePrizeById);

module.exports = router;