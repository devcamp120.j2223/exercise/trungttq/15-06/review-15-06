const express = require("express");

const {createPrizeHistory , getAllPrizeHistory , getPrizeHistoryById , updatePrizeHistoryById , deletePrizeHistoryById, getAllUserPrizeHistory } = require("../controller/prizeHistoryController");

const router = express.Router();

router.get("/prize-histories", getAllPrizeHistory);
router.get("/prize-user-histories", getAllUserPrizeHistory);
router.get("/prize-histories/:historyId", getPrizeHistoryById );
router.post("/prize-histories",createPrizeHistory);
router.put("/prize-histories/:historyId", updatePrizeHistoryById );
router.delete("/prize-histories/:historyId", deletePrizeHistoryById );

module.exports = router;