const express = require("express");

const {createDiceHistory , getAllDiceHistory , getDiceHistoryById , updateDiceHistoryById , deleteDiceHistoryById, getAllUserDiceHistory } = require("../controller/diceHistoryController");

const router = express.Router();

router.get("/dice-histories", getAllDiceHistory);
router.get("/dices", getAllUserDiceHistory);
router.get("/dice-histories/:diceHistoryId", getDiceHistoryById );
router.post("/dice-histories",createDiceHistory);
router.put("/dice-histories/:diceHistoryId", updateDiceHistoryById );
router.delete("/dice-histories/:diceHistoryId", deleteDiceHistoryById );

module.exports = router;