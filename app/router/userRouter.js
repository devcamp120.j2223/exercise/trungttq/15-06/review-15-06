const express = require("express");

const {createUser, getAllUser, getUserById, updateUserById, deleteUserById} = require("../controller/userController");

const router = express.Router();

router.get("/users", getAllUser);
router.get("/users/:userid", getUserById);
router.post("/users",createUser);
router.put("/users/:userid", updateUserById);
router.delete("/users/:userid", deleteUserById);

module.exports = router;