const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const diceHistoryModel = new Schema({
    _id: {
        type: mongoose.Types.ObjectId
        
    },
    user: {
        type: mongoose.Types.ObjectId,
        ref: "user",
        required: true
    },
    dice: {
        type: Number,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    }
});

module.exports = mongoose.model("dicehistory", diceHistoryModel);