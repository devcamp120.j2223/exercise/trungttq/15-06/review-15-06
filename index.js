const express = require("express");
const userModel = require("./app/models/userModel");
const diceHistoryModel = require("./app/models/diceHistoryModel");
const prizeModel = require("./app/models/prizeModel");
const voucherModel = require("./app/models/voucherModel");
const prizeHistoryModel = require("./app/models/prizeHistoryModel");
const voucherHistoryModel = require("./app/models/voucherHistoryModel");

const userRouter = require("./app/router/userRouter");
const diceHistoryRouter = require("./app/router/diceHistoryRouter");
const prizeRouter = require("./app/router/priceRouter");
const voucherRouter = require("./app/router/voucherRouter");
const prizeHistoryRouter = require("./app/router/prizeHistoryRouter");
const voucherHistoryRouter = require("./app/router/voucherHistoryRouter");
const app = express();
const path = require("path");
const mongoose = require("mongoose");
const port = 8000;

app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));

mongoose.connect("mongodb://localhost:27017/CRUD_DICE", (err) => {
    if(err){
        throw err
    }else{
        console.log("Connect Mongodb Successfully");
    }
});

app.use(express.static("views"));

app.use((req, res, next) => {
    console.log(`Time`, new Date());
    next();
});

app.use((req, res, next) => {
    console.log(`Req's method : ${req.method}`);
    next();
});

app.get("/", (req, res) => {
    res.status(200).sendFile(path.join(__dirname + "/views/23.40.html"))
});



app.use("/", userRouter);
app.use("/", diceHistoryRouter);
app.use("/", prizeRouter);
app.use("/", voucherRouter);
app.use("/", prizeHistoryRouter);
app.use("/", voucherHistoryRouter);

app.listen(port, () => {
    console.log(`app running at port ${port}`);
})